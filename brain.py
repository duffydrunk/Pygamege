import numpy
from astar import Astar
from defaults import * 

class Brain():
    
    def __init__(self):
        
        self.map = MAP
        self.ninja_pos = (1*TILESIZE,1*TILESIZE) 
        self.ninja_path = Astar(10,16)
        if self.ninja_path:
            self.ninja_path.reverse()
            self.ninja_path.pop(0)
        self.panel_pos = (1*TILESIZE,3*TILESIZE)
        self.panel_path = Astar(2,16)
        if self.panel_path:
            self.panel_path.reverse()
            self.panel_path.pop(0)
        self.panel_rot = 80
        self.projectile_direction = None
        self.possible_positions = np.where(MAP_x == 1)
            
# path = Astar(2, 16,True)
# path.reverse()
# path.pop(0)