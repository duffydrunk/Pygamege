# -*- coding: utf-8 -*-
import pygame
import random
from tile import Tile
from ninja import Ninja
from projectile import Projectile
from target import Target
from panel import Panel
from brain import Brain
from defaults import *

class Level:
    """
    Class of current game level.
    """
    def __init__(self):
        
        #Get the display surface
        self.display_surface = pygame.display.get_surface()
        
        #Define sprite groups
        self.visible_sprites = pygame.sprite.Group() #Sprites which are always drawn on the screen
        self.obstacle_sprites = pygame.sprite.Group() #Sprites which cause specific actions if collision occours
        self.player_sprites = pygame.sprite.Group() #Sprites which can spawn attack sprites
        self.target_sprites = pygame.sprite.Group() #Sprites which can be killed
        self.attack_sprites = pygame.sprite.Group() #Sprites which can kill killable sprites
        self.mirror_sprites = pygame.sprite.Group() #Sprites that can reflect the projectile
        
        self.create_map() #Create Map
        self.brain = Brain()
    
    def create_map(self):
        """
        Function that creates initial map according to values from defaults.py
        """
        #Check the map matrix from defaults.py
        for r_index, row in enumerate(MAP):
            for c_index, column in enumerate(row):
                x = c_index * TILESIZE
                y = r_index * TILESIZE  
                
                #Create sprites according to map's index
                if column == 'x': 
                    Tile((x,y),[self.visible_sprites,self.obstacle_sprites])
                elif column == 'n':
                    self.ninja = Ninja((x,y),[self.visible_sprites,self.player_sprites],self.obstacle_sprites,self.create_projectile)
                elif column == 't':
                    Target((x,y),[self.visible_sprites,self.target_sprites])
                elif column == 'p':
                    self.panel = Panel((x,y),[self.visible_sprites,self.mirror_sprites],self.obstacle_sprites)
                else:
                    pass
    
    def create_projectile(self, mouse_pos):
        """
        Function that creates projectiles from ninja to the mouse cursor direciton.
        """
        
        ninja_pos = pygame.math.Vector2(self.ninja.rect.center) #Form ninja position as a vector
        direction = (mouse_pos-ninja_pos).normalize() #Find the direction vector between ninja and mouse cursor, and normalize it
        Projectile((self.ninja.rect.x, self.ninja.rect.y),[self.visible_sprites, self.attack_sprites],self.obstacle_sprites,direction,self.mirror_sprites) #Create projectlie
        
    def draw_line(self):
        """
        Function that draws the trajactory of the projectiles
        """
        if self.attack_sprites: #If any projectile is currently in the game
            for sprite in self.attack_sprites: #For every projectile
                lines = sprite.track.copy() #Copy the track list of the projectile 
                lines[-1].append(sprite.rect.center) #Add current position of the projectile to the copy of the track list
                for line in lines:
                    pygame.draw.lines(self.display_surface,(255,0,255), False, line, width= 3) #Draw the tracjactory of the projectile
    
    def hitting_condition(self):
        """
        Function that checks if a projectile collides with a target. If so, both of the target and projectile are killed.
        """
        if self.attack_sprites: #If there are any projectiles on the game
            for attack_sprite in self.attack_sprites: #For every projectile 
                collision_sprites = pygame.sprite.spritecollide(attack_sprite, self.target_sprites, False) #Check if projectile collides with a target
                if collision_sprites: #If collision occurred
                    for sprite in collision_sprites: #Kill both projectile and target
                        sprite.kill()
                    attack_sprite.kill()
                                             
    def create_targets(self,max_iteration = 1000):
        """
        Function that creates new targets when the total nubmer of targets are lower than min target number.
        """
        iter_number = 0 #Current iteration number
        possible_positions = [(i, j) for i in range(ROW_NUMBER) for j in range(COLUMN_NUMBER)] #Possible positions of the new target
        proper = True #Flag for proper place of the new target
        while len(self.target_sprites) < MIN_TARGET_NUM: #While a new target is needed
            new_target_pos = random.choice(possible_positions) #Select a random possible position for the new target
            new_target = pygame.Rect((new_target_pos[1] * TILESIZE,new_target_pos[0] * TILESIZE),(64,64)) #Create a rect object at the selected position

            for sprite in self.visible_sprites: #For every target (Since we don't want targets to overlap)
                if sprite.rect.colliderect(new_target): #Check if there is collision 
                    possible_positions.remove(new_target_pos) #If so, selected position is not proper remove from the list
                    proper = False #Reverse the flag
                    break #No more iteration is needed
                        
            if proper: #If current selected position still proper create new target
                Target((new_target_pos[1] * TILESIZE,new_target_pos[0] * TILESIZE),[self.visible_sprites,self.target_sprites]) #Create new target
                
            if iter_number < max_iteration: #Check iteration number to prevent stucking in a infinite loop
                iter_number += 1
                proper = True #Reverse the flag to select a new possible position if needed

            else: #There is no possible place to place a new target
                return  

    def score(self):
        pass
    
    
    def brain_decides(self):
        # self.ninja.new_pos = self.brain.ninja_pos
        self.ninja.path = self.brain.ninja_path
        self.panel.path , self.panel.new_rot = self.brain.panel_path, self.brain.panel_rot
    

    def run(self):
        #Update and draw the game
        self.visible_sprites.draw(self.display_surface)
        if len(self.target_sprites) < MIN_TARGET_NUM:
            self.create_targets()
        self.visible_sprites.update()
        self.hitting_condition()
        self.draw_line()
        # self.brain_decides()
