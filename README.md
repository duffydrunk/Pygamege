# Pygamege

A ninja tries to destroy the "X" marks by throwing shurikens. Shurikens can bounce off snowy trees up to 10 times, but perish when they hit normal trees or their 11th hit on snowy trees. If the total number of targets on the map is less than 5, new targets will spawn randomly.

Controls: "Arrow Keys" to move & "Left-mouse click" to throw Shuriken
