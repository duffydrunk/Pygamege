# -*- coding: utf-8 -*-
import pygame
from defaults import * 

class Panel(pygame.sprite.Sprite):
    """
    Class of reflective obstacles
    """
    def __init__(self,pos, groups,obstacle_sprites):
        super().__init__(groups)
        self.image = pygame.image.load("Arrow.png").convert_alpha() #Import tileset
        self.image = self.image.subsurface([0,0,13,13]) #Take snowy tree tile from tileset
        self.image = pygame.transform.smoothscale(self.image, (32,32)) #Scale tile to tile size    
        self.rect = self.image.get_rect(topleft = (pos[0]+16,pos[1]+16)) #Define position
        self.hitbox = self.rect.inflate(-6,-6) #Define hitbox of the panel
        self.original_image = self.image.copy() #Keep original image of the panel
        
        # movement defaults 
        self.direction = pygame.math.Vector2() #Define a vector
        self.reflect_direction = pygame.math.Vector2(0,1)
        self.speed = 5 #Define speed
        self.rotation = 0 #Define current rotation
        self.rotation_speed = 1 #Define rotation speed as degree per action
 

        self.rotated = False #False if panel currently available to rotate, otherwise True
        self.rotation_cd = 10 #Minimum required time interval between rotation moves of the panel
        self.rotation_time = 0 #Constant for tracking time interval between rotation moves
        
        self.obstacle_sprites = obstacle_sprites #Define obstacle sprites
    
        self.new_pos = self.hitbox.center
        self.new_rot = None
        self.new_destination = False
        self.path = None
   
    def keyboard_input(self):
        """
        Function that enables ninja to take keyboard inputs
        """
        keys = pygame.key.get_pressed() #Get keyboard keys that are pressed during last event
        
        # Define direction of the specific keys
        if keys[pygame.K_w]:
            self.direction.y = -1
 
        elif keys[pygame.K_s]:
            self.direction.y = 1     
            
        else:
            self.direction.y = 0
 
        if keys[pygame.K_d]:
            self.direction.x = 1

        elif keys[pygame.K_a]:
            self.direction.x = -1
        
        else:
            self.direction.x = 0
            
        if keys[pygame.K_q]:
            self.key_board_rotate(1)
            
        if keys[pygame.K_e]:
            self.key_board_rotate(-1)
            
    # def mouse_input(self):
    #     """
    #     Function that enables ninja to take mouse inputs
    #     """
    #     mouse_pos = pygame.mouse.get_pos() #Get current mouse position
    #     mouse_clicks = pygame.mouse.get_pressed() #Get mouse keys that are pressed during last event

    #     if mouse_clicks[2] and not self.shiruken_throwed: #If right button of the mouse is clicked and panel is available to rotate
    #         mouse_pos = pygame.math.Vector2([mouse_pos[0],mouse_pos[1]]) #Form mouse position vector
    #         self.create_projectile(mouse_pos) #Throw projectile
    #         self.rotated = True #Flag is reversed to prevent projectile spamming
    #         self.rotation_time = pygame.time.get_ticks() # Save the projectile's throwing time 
            
 
    def move(self,speed):
        """
        Function that moves the ninja.
        """
        if self.direction.magnitude() != 0: # Normalize the direction vector if it's not
            self.direction = self.direction.normalize()
            
        self.hitbox.x += self.direction.x * speed #Move hitbox at x-axis
        self.collision("horizontal") #Check if there are any collisions
        
        self.hitbox.y += self.direction.y * speed #Move hitbox at y-axis
        self.collision("vertical") #check if there are any collisions
        self.rect.center = self.hitbox.center #Update rectangle's center as the hitbox's cetenr
        
        if self.new_destination:            
            self.path.pop(0)
            if self.path:
                self.new_pos = (self.path[0][1]*TILESIZE+TILESIZE/2,self.path[0][0]*TILESIZE+TILESIZE/2)
                
            self.new_destination = False

    def key_board_rotate(self,sign):
        """
        Function that rotates the panel from the keyboard input.
        """
        
        if not self.rotated: #If panel is available to rotate
            rotated_image = pygame.transform.rotate(self.original_image, self.rotation) #Rotate original image as desired angle
            new_rect = rotated_image.get_rect(center = self.hitbox.center) #Find new containing rectangle
            new_hitbox = new_rect.inflate(-5,-5) #Define new  hitbox of the panel
            new_hitbox.center = self.hitbox.center #Prevent shift to define new hitbox's center as previous center
            
            for sprite in self.obstacle_sprites: #Itarete over every obstacle
                if sprite.hitbox.colliderect(new_hitbox): #Check if there any collisions that prevent the rotation move of the panel
                    self.rotated = True #Start cooldown for rotation move
                    self.rotation_time = pygame.time.get_ticks() # Save the panel's rotation time
                    return #End the function without rotating the panel due to collision with the obstacle
                
            #There are no collisions        
            self.image = rotated_image #Update hitbox with rotated one
            self.rect = new_rect #Update hitbox with rotated one
            self.hitbox = new_hitbox #Update hitbox with rotated one
            self.rotation += self.rotation_speed*sign #Update rotation angle

            self.reflect_direction = pygame.math.Vector2.rotate(self.reflect_direction, -1*sign*self.rotation_speed) #Update reflection direction

            
            self.rotated = True #Start cooldown for rotation move
            self.rotation_time = pygame.time.get_ticks() # Save the panel's rotation time
            
            
    def collision(self,direction):
                
        """
        Function that checks collision on the x or y axis. If a collision occurs with an obstacle sprite, ninja cannot move towards the collieded obstacle.
        """
        
        if direction == 'horizontal':
            for sprite in self.obstacle_sprites:
                if sprite.hitbox.colliderect(self.hitbox):
                    if self.direction.x > 0: # Panel moving right 
                        self.hitbox.right = sprite.hitbox.left
                    else: # Panel moving left 
                        self.hitbox.left = sprite.hitbox.right

        if direction == 'vertical':
            for sprite in self.obstacle_sprites:
                if sprite.hitbox.colliderect(self.hitbox):
                    if self.direction.y > 0: # Panel moving right 
                        self.hitbox.bottom = sprite.hitbox.top
                    else: # Panel moving left 
                        self.hitbox.top = sprite.hitbox.bottom     


    def brain_rotates(self):
        if self.new_rot and self.new_rot != self.rotation:
            self.rotation %= 360
            self.image = pygame.transform.rotate(self.original_image, self.rotation) #Rotate original image as desired angle
            self.rect = self.image.get_rect(center = self.hitbox.center) #Find new containing rectangle
            new_hitbox = self.rect.inflate(-6,-6) #Define new  hitbox of the panel
            new_hitbox.center = self.hitbox.center #Prevent shift to define new hitbox's center as previous center
            
            self.hitbox = new_hitbox #Update hitbox with rotated one
            sign = -1 if self.new_rot > 180 else 1
            self.rotation += self.rotation_speed*sign #Update rotation angle

            self.reflect_direction = pygame.math.Vector2.rotate(self.reflect_direction, -1*sign*self.rotation_speed) #Update reflection direction
        

    def brain_moves(self):
        if self.path:
            self.new_pos = (self.path[0][1]*TILESIZE+TILESIZE/2,self.path[0][0]*TILESIZE+TILESIZE/2)
            panel_pos = pygame.math.Vector2(self.hitbox.center) #Form ninja position as a vector
            new_pos = pygame.math.Vector2(self.new_pos) #Form final ninja position as a vector
            self.direction = (new_pos-panel_pos) #Find the direction vector between ninja and mouse cursor, and normalize it
            if self.direction.magnitude() < 5:
                self.new_destination = True
            if self.direction.magnitude() > 0: #If there is a vector normalize it
                self.direction = self.direction.normalize()



    def cooldown(self):
        """
        Function that checks if ninja is available to throw projectiles.
        """
        current_time = pygame.time.get_ticks() #Save the current time
        if self.rotated == True: #Check if panel is unavailable to rotate 
            if current_time - self.rotation_time >= self.rotation_cd: #IF defined cooldown time has passed since last rotation
                self.rotated = False #Reverse the flag value to make panel available to rotate again

    def update(self):
        """
        Update method for the ninja class. 
        """
        self.keyboard_input()
        # self.mouse_input()
        self.cooldown()
        self.brain_rotates()
        self.brain_moves()
        self.move(self.speed)
