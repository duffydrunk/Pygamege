# -*- coding: utf-8 -*-
import pygame
from defaults import * 
from projectile import Projectile


class Ninja(pygame.sprite.Sprite):
    """
    Class that creates a ninja chracter that can throw projectiles that can reflect from reflective obstacles
    """
    def __init__(self,pos, groups, obstacle_sprites, create_projectile):
        super().__init__(groups)
        
        #Take Asset from the source image
        self.image = pygame.image.load("SpriteSheet.png").convert_alpha() #Import image
        self.image = self.image.subsurface([0,0,16,16]) #Take the asset
        self.image = pygame.transform.smoothscale(self.image, (TILESIZE,TILESIZE)) #Scale asset to tile size    
        self.rect = self.image.get_rect(topleft = pos) #Define position of the ninja at the map
        self.hitbox = self.rect.inflate(-4,-4) #Define hitbox of the ninja
        
        # movement defaults 
        self.direction = pygame.math.Vector2() #Define a vector
        self.speed = 5 #Define speed
        self.shiruken_throwed = False #False if ninja currently available to throw projectile, otherwise True
        self.shiruken_cd = 1000 #Minimum required time interval between throwing of two projectiles
        self.shiruken_time = 0 #Constant for tracking time interval between throwing projectiles
        
        self.obstacle_sprites = obstacle_sprites #Define obstacle sprites
        self.create_projectile = create_projectile #Inherit the create_projectile method from Level
        
        self.new_pos = self.hitbox.center
        self.new_destination = False
        self.path = None
        
    def keyboard_input(self):
        """
        Function that enables ninja to take keyboard inputs
        """
        keys = pygame.key.get_pressed() #Get keyboard keys that are pressed during last event
        
        # Define direction of the specific keys
        if keys[pygame.K_UP]:
            self.direction.y = -1
 
        elif keys[pygame.K_DOWN]:
            self.direction.y = 1     
            
        else:
            self.direction.y = 0
 
        if keys[pygame.K_RIGHT]:
            self.direction.x = 1

        elif keys[pygame.K_LEFT]:
            self.direction.x = -1
        
        else:
            self.direction.x = 0
   
    def mouse_input(self):
        """
        Function that enables ninja to take mouse inputs
        """
        mouse_pos = pygame.mouse.get_pos() #Get current mouse position
        mouse_clicks = pygame.mouse.get_pressed() #Get mouse keys that are pressed during last event

        if mouse_clicks[0] and not self.shiruken_throwed: #If self button of the mouse is clicked and ninja available to throw projectile
            mouse_pos = pygame.math.Vector2([mouse_pos[0],mouse_pos[1]]) #Form mouse position vector
            self.create_projectile(mouse_pos) #Throw projectile
            self.shiruken_throwed = True #Flag is reversed to prevent projectile spamming
            self.shiruken_time = pygame.time.get_ticks() # Save the projectile's throwing time
    
    def cooldown(self):
        """
        Function that checks if ninja is available to throw projectiles.
        """
        current_time = pygame.time.get_ticks() #Save the current time
        if self.shiruken_throwed == True: #Check if ninja is unavailable to throw projectile
            if current_time - self.shiruken_time >= self.shiruken_cd: #IF defined cooldown time has passed since last throw
                self.shiruken_throwed = False #Reverse the flag value to make ninja available to throw again
    
    def move(self,speed):
        """
        Function that moves the ninja.
        """
        if self.direction.magnitude() != 0: # Normalize the direction vector if it's not
            self.direction = self.direction.normalize()
            
        self.hitbox.x += self.direction.x * speed
        self.collision("horizontal")
        
        self.hitbox.y += self.direction.y * speed
        self.collision("vertical")
        self.rect.center = self.hitbox.center
        
        if self.new_destination:            
            self.path.pop(0)
            if self.path:
                self.new_pos = (self.path[0][1]*TILESIZE+TILESIZE/2,self.path[0][0]*TILESIZE+TILESIZE/2)
                
            self.new_destination = False
                
    
    def collision(self,direction):
                
        """
        Function that checks collision on the x or y axis. If a collision occurs with an obstacle sprite, ninja cannot move towards the collieded obstacle.
        """
        
        if direction == 'horizontal':
            for sprite in self.obstacle_sprites:
                if sprite.hitbox.colliderect(self.hitbox):
                    if self.direction.x > 0: # Ninja moving right 
                        self.hitbox.right = sprite.hitbox.left
                    else: # Ninja moving left 
                        self.hitbox.left = sprite.hitbox.right

        if direction == 'vertical':
            for sprite in self.obstacle_sprites:
                if sprite.hitbox.colliderect(self.hitbox):
                    if self.direction.y > 0: # Ninja moving right 
                        self.hitbox.bottom = sprite.hitbox.top
                    else: # Ninja moving left 
                        self.hitbox.top = sprite.hitbox.bottom
                        
    def brain_moves(self):
        if self.path:
            self.new_pos = (self.path[0][1]*TILESIZE+TILESIZE/2,self.path[0][0]*TILESIZE+TILESIZE/2)
            ninja_pos = pygame.math.Vector2(self.hitbox.center) #Form ninja position as a vector
            new_pos = pygame.math.Vector2(self.new_pos) #Form final ninja position as a vector
            self.direction = (new_pos-ninja_pos) #Find the direction vector between ninja and mouse cursor, and normalize it
            if self.direction.magnitude() < 5:
                self.new_destination = True
            if self.direction.magnitude() > 0: #If there is a vector normalize it
                self.direction = self.direction.normalize()

        
    def update(self):
        """
        Update method for the ninja class. 
        """
        self.keyboard_input()
        self.mouse_input()
        self.cooldown()
        self.brain_moves()
        self.move(self.speed)
        
        
