# -*- coding: utf-8 -*-
import pygame, sys
from defaults import *
from level import Level

class Main:
    
    def __init__(self):
        
        pygame.init() #Initialize the pygame
        self.screen = pygame.display.set_mode((WIDTH,HEIGHT)) #Create screen
        pygame.display.set_caption("Beamer") #Set screen caption
        self.clock = pygame.time.Clock() #Create an object to help track time
        self.level = Level() 
        
    def run(self):
        """
        This is the loop function that updates the screen, according to the events happened during
        the cycle.
        """
        while True: 
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit() #If game is stopped quit pypame
                    sys.exit()
                
            self.screen.fill("black") #Main background
            self.level.run() #Initialize the level and perform actions
            pygame.display.update() #Display new events on the screen
            self.clock.tick(FPS) #Limit the run time speed of the game to FPS value
            
if __name__ == "__main__":
    game = Main()
    game.run()
